# This is a generated file; DO NOT EDIT!
#
# Please edit 'setup.cfg' to modify top-level dependencies and run
# './contrib/refresh-requirements.sh' to regenerate this file

Logbook==1.8.0
Represent==2.1
anyio==4.8.0
attrs==24.3.0
certifi==2024.12.14
charset-normalizer==3.4.1
h11==0.14.0
httpcore==1.0.7
httpx==0.28.1
idna==3.10
importlib_metadata==8.6.1
requests==2.32.3
rush==2021.4.0
sgp4==2.23
sniffio==1.3.1
spacetrack==1.3.1
typing_extensions==4.12.2
urllib3==2.3.0
zipp==3.21.0
