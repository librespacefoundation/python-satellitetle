import unittest

from pathlib import Path
from unittest import mock
from satellite_tle import fetch_tles_from_url, get_tle_sources


def bobcat1_tle():
    return (
        "BOBCAT-1                \n"
        "1 46922U 98067RS  21110.83931133  .00021294  00000-0  32302-3 0  9990\n"
        "2 46922  51.6423 262.4338 0001752 235.0482 125.0345 15.54552748 25871\n")


def mock_requests_response(text):
    response = mock.MagicMock()
    response.raise_for_status = mock.Mock()
    response.text = text
    response.content = text.encode('ascii')
    return response


class TestTlesFromUrl(unittest.TestCase):
    '''
    Unit tests for 'satellite_tle.fetch_tles_from_url'
    '''

    @mock.patch('satellite_tle.fetch_tle.requests')
    def test_remote(self, requests):
        text = bobcat1_tle()
        requests.get.return_value = mock_requests_response(text)

        url = "https://www.example.com/mocked/tle_file_fixture.txt"
        tles = fetch_tles_from_url(url=url)
        assert 46922 in tles.keys()
        assert len(tles) == 1

    @mock.patch('satellite_tle.fetch_tle.requests')
    def test_remote_trailing_newline(self, requests):
        text = bobcat1_tle() + '\n'
        requests.get.return_value = mock_requests_response(text)

        url = "https://www.example.com/mocked/tle_file_fixture.txt"
        tles = fetch_tles_from_url(url=url)
        assert 46922 in tles.keys()
        assert len(tles) == 1

    def test_local(self):
        abs_path = Path(__file__).parent / '..' / 'fixtures' / 'iss.txt'
        tles = fetch_tles_from_url(f'file://{abs_path}')

        assert 25544 in tles.keys()
        assert isinstance(tles[25544], tuple)
        assert len(tles[25544]) == 3

    def test_local_zip(self):
        abs_path = Path(__file__).parent / '..' / 'fixtures' / 'elements.zip'
        tles = fetch_tles_from_url(f'file://{abs_path}')

        print(tles)
        assert len(tles.keys()) == 2
        assert 44878 in tles.keys()
        assert 25544 in tles.keys()


class TestGetTleSource(unittest.TestCase):
    def test_get_tle_sources(self):
        sources = get_tle_sources()
        self.assertIsInstance(sources, list)
        self.assertTrue(len(sources) > 0)
        for source in sources:
            self.assertIsInstance(source, tuple)
            self.assertIsInstance(source[0], str)
            self.assertIsInstance(source[1], str)
